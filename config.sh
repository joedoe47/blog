#Mksite V2 Configuration

SITE_NAME="joedoe47/blog"

SITE_URL="https://git.joepcs.com/blog"
#SITE_URL="http://127.0.0.1:8080"

#leave commented if multiple users make posts
#AUTHOR="John Doe"

GRAVATAR_SIZE="90"

#add http freindly url if you use SSH
GIT_SOURCE="https://gitlab.com/joedoe47/blog"

#files that you want copied over to the output folder
STATIC_FILES=(
"htm"
"html"
"css"
"js"
"jpeg"
"jpg"
"png"
"gif"
"bmp"
"svg"
)
