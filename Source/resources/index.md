## resources

I figured it would be nice to share some resources that I use to keep up to date and use in nor particular order. Hopefully the list isn't excessively long.

if you have any suggestions; I'd be glad to take a look at any resources you have to offer.


I will try to keep this list up to date, last update {{DATE}}. However I have a git repository with some bookmarks I keep on hand, [here](http://git.joepcs.com/unlisted/links/index.html).

- [lxer](http://lxer.com)

    - a humble and independent publishing site managed by a few well known FOSS people. No ads. Users can contribute content.

- [devops'ish](https://devopsish.com)

    - a blog about devops, cloud, and open source in general by an individual known as chris short. 

- [opensource.com](https://opensource.com)

    - a blog run by several people that are passionate about open source and are professionals in the fields they write about.

- [Security now podcast](https://twit.tv/shows/security-now)

    - a podcast hosted by Steve Gibson and Leo Laporte. They talk about publicly disclosed zero days and share any tips they find about how to have better operational security as well as how some exploits in particular work or why they can be so dangerous and far reaching.

- [eff deeplinks](https://www.eff.org/deeplinks)

    - a site hosted by the electronic fronteir foundation. they talk about recent issues around the world about issues that affect our privacy and or intellectual property rights.

- [rosettacode](https://rosettacode.org/wiki/Rosetta_Code)

    - this site shows how to do certain programing techniques in other laguanges. Really useful for refrence.

- [treehouse - youtube](https://www.youtube.com/user/gotreehouse) 

    - a podcast hosted by contributers to treehouse where they show how to program in a certain language and how to also how to do some interesting things with code. (I particularly like series they do with web development but they have more than just that)

- [linux academy](https://linuxacademy.com)

    - a freemium service that shows how to get certified with linux, devops, containers, and several other open source utilities. 

- [free code camp - youtube](https://www.youtube.com/channel/UC8butISFwT-Wl7EV0hUK0BQ)

    - a youtube channel that shows indepth how to get started with certain languages. I really like how the series gives working examples and walks through whatever language they are doing.

- [derek banas - youtube](https://www.youtube.com/user/derekbanas)

    - Derek shows makes concise videos about the run down on programming languages to get started quick. Its helped me at least get the basics down to at least be able to read some new code and know whats happening. 

- [traversy media - youtube](https://www.youtube.com/user/TechGuyWeb)

    - goes indepth as to how to make functional programs in new languages or practice and they also have some good talks about how to be a better programmer.
 
- [coding tech - youtube](https://www.youtube.com/channel/UCtxCXg-UvSnTKPOzLH4wJaQ) 

    - has some nice high level talks about technology that corporations use 



