## progress

> **![joedoe47 Gravatar](https://s.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=90)**

> By joedoe47 on Sat 23 May 2020 03:19:50 AM UTC

> Tags: 

----------

<div align="center" style="font-family: monospace, fixed; font-weight: bold;">
<div style="font-family: monospace, fixed; font-weight: bold;">
<span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;;8.&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;.&#160;&#160;.&#160;.&#160;</span><span style=";color:#0aa;background-color:#555">&#160;</span><span style=";color:#555;background-color:#000">S</span><span style=";color:#aaa;background-color:#fff">X&#160;.&#160;&#160;.&#160;&#160;.</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;.&#160;&#160;&#160;&#160;</span><span style=";color:#555;background-color:#aaa">.</span><span style=";color:#00a;background-color:#000">.</span><span style=";color:#0a0;background-color:#000">&#160;</span><span style=";color:#a50;background-color:#555">t</span><span style=";color:#aaa;background-color:#fff">;&#160;&#160;&#160;&#160;.&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;&#160;&#160;.&#160;</span><span style=";color:#fff;background-color:#aaa">8</span><span style=";color:#0a0;background-color:#000">:</span><span style=";color:#a0a;background-color:#555">&#160;</span><span style=";color:#00a;background-color:#000">.&#160;</span><span style=";color:#555;background-color:#aaa">8</span><span style=";color:#aaa;background-color:#fff">.&#160;&#160;&#160;&#160;&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;.&#160;&#160;&#160;&#160;;</span><span style=";color:#000;background-color:#555">8</span><span style=";color:#555;background-color:#aaa">X</span><span style=";color:#aaa;background-color:#fff">&#160;</span><span style=";color:#a0a;background-color:#555">t</span><span style=";color:#0a0;background-color:#000">&#160;</span><span style=";color:#00a;background-color:#000">&#160;</span><span style=";color:#fff;background-color:#aaa">@</span><span style=";color:#aaa;background-color:#fff">.&#160;.&#160;.&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;.&#160;&#160;</span><span style=";color:#a0a;background-color:#555">&#160;:</span><span style=";color:#aaa;background-color:#fff">:.@</span><span style=";color:#0a0;background-color:#000">t</span><span style=";color:#00a;background-color:#000">.</span><span style=";color:#555;background-color:#000">8</span><span style=";color:#aaa;background-color:#fff">;&#160;&#160;&#160;&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;.&#160;&#160;</span><span style=";color:#555;background-color:#aaa">t</span><span style=";color:#555;background-color:#000">X</span><span style=";color:#aaa;background-color:#fff">S&#160;&#160;&#160;</span><span style=";color:#fff;background-color:#aaa">:</span><span style=";color:#a00;background-color:#000">&#160;&#160;</span><span style=";color:#a0a;background-color:#555">&#160;</span><span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;&#160;.</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;&#160;</span><span style=";color:#fff;background-color:#aaa">8</span><span style=";color:#0a0;background-color:#000">:</span><span style=";color:#fff;background-color:#aaa">8</span><span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;&#160;&#160;</span><span style=";color:#a0a;background-color:#555">&#160;</span><span style=";color:#00a;background-color:#000">:</span><span style=";color:#a00;background-color:#000">.</span><span style=";color:#555;background-color:#aaa">&#160;</span><span style=";color:#aaa;background-color:#fff">&#160;.&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;.t</span><span style=";color:#555;background-color:#000">8</span><span style=";color:#555;background-color:#aaa">S</span><span style=";color:#aaa;background-color:#fff">&#160;.&#160;.&#160;&#160;.</span><span style=";color:#555;background-color:#000">8</span><span style=";color:#0a0;background-color:#000">.</span><span style=";color:#a00;background-color:#000">;</span><span style=";color:#aaa;background-color:#fff">S&#160;&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;</span><span style=";color:#a0a;background-color:#555">&#160;.</span><span style=";color:#aaa;background-color:#fff">.&#160;&#160;&#160;&#160;&#160;&#160;&#160;</span><span style=";color:#fff;background-color:#aaa">@</span><span style=";color:#00a;background-color:#000">.</span><span style=";color:#0a0;background-color:#000">.</span><span style=";color:#000;background-color:#555">S</span><span style=";color:#aaa;background-color:#fff">:&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;</span><span style=";color:#555;background-color:#aaa">S</span><span style=";color:#555;background-color:#000">8</span><span style=";color:#aaa;background-color:#fff">t&#160;.&#160;&#160;.&#160;&#160;.&#160;</span><span style=";color:#555;background-color:#aaa">8</span><span style=";color:#00a;background-color:#000">&#160;&#160;</span><span style=";color:#555;background-color:#aaa">8</span><span style=";color:#aaa;background-color:#fff">.&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;</span><span style=";color:#fff;background-color:#aaa">X</span><span style=";color:#0a0;background-color:#000">.</span><span style=";color:#a0a;background-color:#555">&#160;</span><span style=";color:#555;background-color:#aaa">SXXXXXXXXXX</span><span style=";color:#00a;background-color:#000">;</span><span style=";color:#0a0;background-color:#000">.</span><span style=";color:#a00;background-color:#000">.</span><span style=";color:#fff;background-color:#aaa">X</span><span style=";color:#aaa;background-color:#fff">&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;</span><span style=";color:#fff;background-color:#aaa">8tt;;;;;;;;t;tt;;8</span><span style=";color:#aaa;background-color:#fff">&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;.&#160;&#160;&#160;&#160;.</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;.&#160;.&#160;&#160;&#160;&#160;&#160;&#160;&#160;.&#160;&#160;&#160;&#160;&#160;.&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#fff">&#160;&#160;&#160;&#160;&#160;.&#160;.&#160;.&#160;&#160;&#160;.&#160;.&#160;&#160;&#160;&#160;</span><br />
</div>
</div>

progress takes time. It sucks because it can feel like nothing changes over a long period of time but life is change. We try and change for the better for X reason, whatever drives you forward. Its important to also look back and reassess the changes you have done because sometimes change we make actually hinder that "X" reason for why you wanted to change in the first place. 

You could want to change because you want to be with a beautiful woman or super handsome guy. Some may change to try and keep up appearances with a new job. Other change because they need or want to change for something bigger than themselves. (eg. changing a bad habit so as not to teach your child the wrong thing)

I look back on this decade and I feel as though I have made some decent change over time and hopefully I can continue with education, work, friends, love life, etc over time. 

Well here is to reflecting on the past and looking and moving forward and full speed. :)


