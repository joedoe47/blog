## UX_UI_Design

> **![joedoe47 Gravatar](https://s.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=90)**

> Author: joedoe47

> Date: Wed 14 Nov 2018 01:14:36 PM UTC

> Tags: UX, CSS, Front end 

----------

<div style="font-family: monospace, fixed; font-weight: bold;" align="center">
<span style=";color:#555;background-color:#00a">@8@8@8</span><span style=";color:#0aa;background-color:#00a">S</span><span style=";color:#555;background-color:#00a">@8@8@</span><br />
<span style=";color:#0aa;background-color:#55f">8S</span><span style=";color:#55f;background-color:#0aa">8</span><span style=";color:#0aa;background-color:#55f">;</span><span style=";color:#55f;background-color:#0aa">8</span><span style=";color:#0aa;background-color:#55f">;</span><span style=";color:#aaa;background-color:#0aa">X</span><span style=";color:#0aa;background-color:#55f">&#160;</span><span style=";color:#5ff;background-color:#0aa">:</span><span style=";color:#0aa;background-color:#55f">&#160;</span><span style=";color:#5ff;background-color:#0aa">:</span><span style=";color:#00a;background-color:#55f">t</span><br />
<span style=";color:#0aa;background-color:#00a">8</span><span style=";color:#0aa;background-color:#55f">.</span><span style=";color:#aaa;background-color:#fff">%:;t.&#160;&#160;@</span><span style=";color:#5ff;background-color:#55f">8</span><span style=";color:#555;background-color:#0aa">8</span><br />
<span style=";color:#0aa;background-color:#00a">X</span><span style=";color:#55f;background-color:#0aa">8</span><span style=";color:#0aa;background-color:#55f">.</span><span style=";color:#aaa;background-color:#0aa">X</span><span style=";color:#0aa;background-color:#55f">&#160;:</span><span style=";color:#aaa;background-color:#5ff">@</span><span style=";color:#0aa;background-color:#55f">&#160;</span><span style=";color:#aaa;background-color:#fff">&#160;</span><span style=";color:#fff;background-color:#aaa">S</span><span style=";color:#5ff;background-color:#55f">X</span><span style=";color:#a0a;background-color:#00a">X</span><br />
<span style=";color:#555;background-color:#00a">@</span><span style=";color:#0aa;background-color:#55f">;</span><span style=";color:#fff;background-color:#aaa">@</span><span style=";color:#aaa;background-color:#fff">X&#160;;t&#160;&#160;</span><span style=";color:#5ff;background-color:#aaa">X</span><span style=";color:#0aa;background-color:#55f">&#160;</span><span style=";color:#0aa;background-color:#555">8</span><br />
<span style=";color:#000;background-color:#00a">8</span><span style=";color:#55f;background-color:#0aa">8</span><span style=";color:#aaa;background-color:#55f">8</span><span style=";color:#fff;background-color:#aaa">S</span><span style=";color:#0aa;background-color:#55f">&#160;</span><span style=";color:#55f;background-color:#0aa">8</span><span style=";color:#5ff;background-color:#55f">8</span><span style=";color:#5ff;background-color:#aaa">X</span><span style=";color:#aaa;background-color:#fff">.</span><span style=";color:#5ff;background-color:#aaa">@</span><span style=";color:#0aa;background-color:#55f">&#160;</span><span style=";color:#555;background-color:#00a">8</span><br />
<span style=";color:#00a;background-color:#000">S</span><span style=";color:#0aa;background-color:#55f">.</span><span style=";color:#5ff;background-color:#aaa">8</span><span style=";color:#aaa;background-color:#fff">X&#160;888;</span><span style=";color:#5ff;background-color:#55f">@</span><span style=";color:#aaa;background-color:#55f">8</span><span style=";color:#00a;background-color:#000">@</span><br />
<span style=";color:#00a;background-color:#000">.</span><span style=";color:#0aa;background-color:#55f">t&#160;.</span><span style=";color:#5ff;background-color:#aaa">8</span><span style=";color:#aaa;background-color:#55f">8</span><span style=";color:#aaa;background-color:#5ff">8</span><span style=";color:#aaa;background-color:#55f">8</span><span style=";color:#5ff;background-color:#55f">@</span><span style=";color:#aaa;background-color:#55f">8</span><span style=";color:#0aa;background-color:#55f">&#160;</span><span style=";color:#0a0;background-color:#000">8</span><br />
<span style=";color:#00a;background-color:#000">&#160;</span><span style=";color:#000;background-color:#555">@</span><span style=";color:#0aa;background-color:#555">8</span><span style=";color:#0aa;background-color:#00a">8</span><span style=";color:#00a;background-color:#55f">8</span><span style=";color:#0aa;background-color:#55f">;&#160;</span><span style=";color:#55f;background-color:#0aa">8</span><span style=";color:#555;background-color:#a0a">8</span><span style=";color:#00a;background-color:#0aa">8</span><span style=";color:#00a;background-color:#555">8</span><span style=";color:#a00;background-color:#000">&#160;</span><br />
</div>

The method that we design things has to be pleasing to the eye but also have accessibility in mind for those with disabilities. 

This were the two reasons of why I designed this site as such. I wanted to have a retro looking blog.

I'm not saying that looks, layouts, colors and asthetics should be minimal by any means. I'm just a fan of old school designs is all. Its just that we should keep in mind that our applications and creations should not forget those who can't necessarily see certain colors, who can't see at all, or whom have some disability that makes their senses work a little differently from us "normal" people. (yes, irony was intended in the quotes because we are all wierd, interesting, and unique individuals)

A good example of colorful designs but with accessibility in mind is the [material design](https://material.io/).

While its not explicitly stated that we should keep colors in mind for people who can not see certain colors. It allows for us to use alternate color pallets. Very nice little detail.

The concept that bootstrap and other frameworks that followed made the web a little more organized. Things like rows and collumn css classes allowed for a more stanard and nice way to view data even in a terminal based browser. (eg. try seeing this site or my back up git repository browser in a text only browser)

 
