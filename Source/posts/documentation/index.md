## documentation

> **![joedoe47 Gravatar](https://s.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=90)**

> Author: joedoe47

> Date: Fri 26 Oct 2018 04:07:40 PM UTC

----------

<div style="font-family: monospace, fixed; font-weight: bold;" align="center">
<span style="color:#fff;">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;@</span><span style="">&#160;</span><span style=";color:#221;background-color:#530">8</span><span style=";color:#542;background-color:#530">8</span><span style=";color:#530;background-color:#221">8</span><span style=";color:#fff;background-color:#221">&#160;8</span><span style=";color:#aaa">;&#160;&#160;&#160;&#160;</span><br />
<span style=";color:#aaa">&#160;&#160;&#160;&#160;.8</span><span style=";color:#fff;background-color:#221">&#160;</span><span style=";color:#555;background-color:#530">8</span><span style=";color:#542;background-color:#530">88888888</span><span style=";color:#555;background-color:#530">8</span><span style=";color:#530;background-color:#221">X</span><span style=";color:#fff;background-color:#221">;</span><span style=";color:#aaa;background-color:#fff">@</span><br />
<span style=";color:#aaa">&#160;</span><span style=";color:#fff;background-color:#221">X</span><span style=";color:#530;background-color:#221">X</span><span style=";color:#542;background-color:#530">88888888888888</span><span style=";color:#530;background-color:#221">X</span><span style=";color:#aaa;background-color:#fff">8</span><br />
<span style=";color:#fff;background-color:#221">:</span><span style=";color:#221;background-color:#530">8</span><span style=";color:#542;background-color:#530">8888888888888</span><span style=";color:#530;background-color:#221">8</span><span style=";color:#fff;background-color:#221">:</span><span style=";color:#aaa">@@</span><span style=";color:#fff;background-color:#221">@</span><br />
<span style=";color:#fff;background-color:#221">;</span><span style=";color:#aaa;background-color:#fff">&#160;S</span><span style=";color:#fff;background-color:#221">:</span><span style=";color:#530;background-color:#221">8</span><span style=";color:#542;background-color:#530">8888888</span><span style=";color:#221;background-color:#530">8</span><span style=";color:#fff;background-color:#221">&#160;</span><span style=";color:#aaa;background-color:#fff">8X</span><span style=";color:#fff;background-color:#221">@:S</span><span style=";color:#aaa;background-color:#fff">;</span><br />
<span style=";color:#aaa;background-color:#fff">:</span><span style=";color:#fff;background-color:#221">88</span><span style=";color:#aaa;background-color:#fff">X&#160;;</span><span style=";color:#fff;background-color:#221">S</span><span style=";color:#530;background-color:#221">@</span><span style=";color:#542;background-color:#530">88</span><span style=";color:#555;background-color:#221">t</span><span style=";color:#fff;background-color:#221">8</span><span style=";color:#aaa;background-color:#fff">X8</span><span style=";color:#fff;background-color:#221">;t</span><span style=";color:#aaa">8&#160;&#160;&#160;</span><br />
<span style=";color:#aaa">&#160;&#160;&#160;%</span><span style=";color:#fff;background-color:#221">88</span><span style=";color:#aaa;background-color:#fff">@&#160;;XX</span><span style=";color:#fff;background-color:#221">S:S</span><span style=";color:#aaa">:&#160;&#160;&#160;&#160;&#160;</span><br />
<span style=";color:#aaa;">&#160;&#160;&#160;&#160;&#160;&#160;t</span><span style=";color:#fff;background-color:#221">8@;t</span><span style=";color:#aaa">X&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</span><br />
</div>


Documentation is very important. It can help programmers get up to speed on a project, it can help new scientists learn about the latest advancements, and help buisnesses in a myriad of ways. Documentation that outline procedures in case of an emergency, documentation that logs interactions with clients, and documentation of projects can help build a sort of buisness intellegence and memory. 

we have to take into consideration the audience and the level of proficency before writing documentation, automated documentation software will miss out on some human things. Tools that document software might for example miss how to word something so that a new programmer can understand what a function does (but it doesn't hurt to try and use tools that help us document as we program).

Your also going to want to have an easy method to write the documentation. Usually people will use markdown, LaTeX, and ReStructured Text to write documentation and each of these mark up languages have their merits. However it really depends on what your needs are. Personally I can see why a lot of people go for markdown (I still use it) its easy to pick up but I also see why others really favor ReStructured Text as it has more features like tables. Or in some cases you might want something more "complete" such as a wiki to fully document your findings. (and for such needs we have tools like ikiwiki, github / gitlab pages, or even full suites like doku wiki)

Updating and maintaining documentation is also very important. While future proofing code is the best way to go. Sometimes large changes that breaks or entirely changes a piece of code or how we access certain libraries, APIs, or any findings we have. We need to be ready for this. This is also why we shouldn't shy away from automated solutions to help in documenting our code, API, ro what have you. We shouldn't allow fast changing code to lead to outdated documentation. It hinders new clients/team members, it leads to features being less used, and can leave people unsure about what exactly is the difference between say like version 1.0.1 of your application and version 1.1.0 of your application. (which could indeed be game changing or have a critical security patch or a more efficent method to do "X")

Documentation also requires verbal or written communication apart from just say like properly commenting your code or documenting your code, app, or service.

Its important to talk to your team and coworkers as to how you plan to implement new features. When will 'X' new feature be released? This is why we use things like kanban boards and have scrum meetings. Its important to keep in communication with your team, the person dedicated to documentation, and your managers up to speed.

Not communication any planned features leads to other catastophic events, such as incompatible versions, late release schedules, and inconsistant productivity. To point out a few small things.

Sometimes having talks or livestreams with potential clients can also help showcase potentially important parts of your documentation or feature and how to correctly use it. 

Case in point I personally love to watch videos of how programmers on treehouse showcase some new way of coding something, I was unaware of and their videos usually get me very interested. The last video I watched involved some coders showing how fast single page applications in javascript can work. They also had a video showcasing the importance of SVGs and how to save on bandwith and HTTP requests. (very interesting stuff indeed that I did not know)

It can be an effective way to also bring entirely new people up to speed if say like the audience for your code, applications, or any other findings require a certain amount of knowledge before hand. 

Example, I am not really a nodejs expert yet. However a video however when I see videos from the [nodejs](https://www.youtube.com/channel/UCQPYJluYC_sn_Qz_XE-YbTQ) youtube channel, they concisely point out how to do new things from my perspective everytime I see one of their videos.

`
