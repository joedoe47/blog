## security

> **![joedoe47 Gravatar](https://s.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=90)**

> Author: joedoe47

> Date: Fri 26 Oct 2018 04:07:18 PM UTC

----------

<div style="font-family: monospace, fixed; font-weight: bold;" align="center">
<span style="">&#160;</span><span style=";color:#aaa;background-color:#ff5">@</span><span style=";color:#a50;background-color:#ff5">t</span><span style=";color:#aaa;background-color:#ff5">8</span><span style=";color:#a50;background-color:#ff5">t</span><span style=";color:#aaa;background-color:#ff5">8</span><span style="">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</span><br />
<span style=";color:#aaa;background-color:#ff5">%</span><span style=";color:#aaa;background-color:#ff5">8</span><span style="">&#160;</span><span style=";color:#aaa;background-color:#ff5">X</span><span style=";color:#aaa;background-color:#ff5">88X</span><span style=";color:#aaa;background-color:#ff5">t</span><span style=";color:#aaa;background-color:#ff5">S</span><span style=";color:#aaa;background-color:#ff5">XSSSSSSSS%</span><span style="">&#160;</span><br />
<span style=";color:#aaa;background-color:#ff5">X</span><span style="">&#160;&#160;</span><span style=";color:#aaa;background-color:#ff5">S</span><span style=";color:#aaa;background-color:#ff5">8@@X</span><span style=";color:#aaa;background-color:#ff5">.</span><span style=";color:#aaa;background-color:#ff5">@</span><span style=";color:#aaa;background-color:#ff5">X</span><span style="">&#160;&#160;&#160;</span><span style=";color:#aaa;background-color:#ff5">X</span><span style=";color:#aaa;background-color:#ff5">;</span><span style="">&#160;</span><span style=";color:#aaa;background-color:#ff5">8</span><span style=";color:#aaa;background-color:#ff5">t</span><span style="">&#160;</span><br />
<span style="">&#160;</span><span style=";color:#aaa;background-color:#ff5">@</span><span style=";color:#aaa;background-color:#ff5">X@X</span><span style=";color:#aaa;background-color:#ff5">8</span><span style="">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</span>
<br><br><br><br><br>
</div>

Operational security is impairative in a world where we are so dependent on technology and the internet. appropriate password lengths and criterias as outlined in [EFF SSD](https://ssd.eff.org/en/module/creating-strong-passwords) help slow down attempts to break into our accounts, encrypted back up drives, and machines. (using stiky notes on your desk and sharing passwords is a really bad idea in most if not all cases)

layering our security is a key thing to prevent our networks from being comprimised. Using different CPU archtectures, selectively adding a mixture of different enviornments, ensuring there is a well documented plan to how these layers in the security are to benefit the company, and ensuring there are ways to actively monitor any network or enviornment abnormalities. Is generally how modern enterprises keep red team (bad guys) at bay.

Now if that all seems like jargon its because security in reality depends on your specific scenario.

Organizations like google for a fact DO use different CPU archetectures or used to anyways; as a means to not just lower their enviornmental footprint but also make it difficult for an attacker to simply write 1 exploit and then successfully take over all windows or all linux 64 bit machines. (however moves like this do come at a cost. So it REALLY depends on your scope and scenario)

When it comes to "mixed eviornments" some companies can also see this as different OSes, managers can also asses that what is needed is just different machines (in the case of computers that operate heavy machinery), or even just two different physical locations even (to store say like back ups offsite)

When it comes to a well documented plan. Well it should go without saying but should be said anyways that there needs to be a plan set to continually upkeep the layered security you have decided to go with. Things like how often to schedule updates to be applied on a large enterprise, how often to do back ups, who do you call when a server needs a UPS or PSU replaced, how often do we upgrade parts and machines. We need to document the little details to make sure the big plans go off with zero and or minimal issues.

Then there is monitoring. Some companies require the 99.999% up time. In these cases your enterprise might require a team dedicated to monitoring uptime of machines, logs, and periodically ensuring that the network is operating at maximum efficency.

All in all, as a manager or network engineer or whatever your position may be in relation to security. Its important to think about how to minimize the red team (bad guys) have to work with in distrupting your organization's productivity.
