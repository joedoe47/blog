## contact

> **![joedoe47 Gravatar](https://s.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=90)**

> Author: joedoe47

> Date: {{DATE}}

 ----------

Please note what I say or do online do not represent any organizations or individuals that I work or associate with.

- IRC

    - freenode: joedoe47

    - gnome.org: joedoe47


- Email

    - joedoe47 {at} gmail {dot} com

- Micro Blogging

    - [joedoe@mastondon.sdf.org](https://mastodon.sdf.org/@joedoe)

    - [twitter.com/jamz3243](https://twitter.com/jamz3243)


- Social Media

    - [joedoe47@diasp.org](https://diasp.org/people/1550fbf0f1360134b19a4986d5cbec7f)

    - [keybase.io/joedoe47](https://keybase.io/joedoe47)
