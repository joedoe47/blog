### privacy


<pre>
  ..---..
 /       \
|         |
:         ;
 \  \~/  /
  `, Y ,'
   |_|_|
   |===|
   |===|
    \_/
<pre>

Privacy is important. 

any code written here is written to respect your privacy just as much as I would want my privacy to be respected.

I rarely use cookies to share on sites but if you do see cookies its usually done by 3rd party advertisers such as google or those who sponser me.

So please be aware of the fact that I might inadvertently apply a cookie this way.

Any javascript I use is documented and hopefully not really necessary to operate, navigate, or use this site. As this site is designed to be used on old mobile phones and even through text based browsers like links2, elinks, and w3m. (I use ASCII text art over images for this very reason)

any user data logged is kept for roughly 1 year and publicly available [here](../../logs/)
